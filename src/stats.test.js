import {strict as assert} from 'assert';
import {browserZoomFactor, pinchZoomFactor} from './stats.js';

describe('Provide only available data', () => {
  describe('`browserZoomFactor`', () => {
    it('WHEN `globalThis.screen.scaleFactor` is NOT available THEN return `null`', () => {
      const mockedGlobalThis = {devicePixelRatio: 1};
      assert.equal(browserZoomFactor({globalThis: mockedGlobalThis}), null);
    });
    it('WHEN `globalThis.devicePixelRatio` is NOT available THEN return `null`', () => {
      const mockedGlobalThis = {screen: {scaleFactor: 1}};
      assert.equal(browserZoomFactor({globalThis: mockedGlobalThis}), null);
    });

    it('WHEN `scaleFactor`+`devicePixelRatio` are given THEN return a browserZoomFactor', () => {
      const mockedGlobalThis = {devicePixelRatio: 10, screen: {scaleFactor: 2}};
      assert.equal(browserZoomFactor({globalThis: mockedGlobalThis}), 10 / 2);
    });

    // not sure this test is (any) good
    it('WHEN no argument passed THEN use the global `globalThis`', () => {
      globalThis = {devicePixelRatio: 12345, screen: {scaleFactor: 678}};
      assert.equal(browserZoomFactor(), 12345 / 678);
    });
  });

  describe('`pinchZoomFactor`', () => {
    it('WHEN `globalThis.visualViewport` does not exist THEN return `null`', () => {
      const mockedGlobalThis = {};
      assert.equal(pinchZoomFactor({globalThis: mockedGlobalThis}), null);
    });
    it('WHEN `globalThis.visualViewport.scale` does not exist THEN return `null`', () => {
      const mockedGlobalThis = {visualViewport: {}};
      assert.equal(pinchZoomFactor({globalThis: mockedGlobalThis}), null);
    });

    it('WHEN `globalThis.visualViewport.scale` exists THEN return as is', () => {
      const mockedGlobalThis = {visualViewport: {scale: 42}};
      assert.equal(pinchZoomFactor({globalThis: mockedGlobalThis}), 42);
    });

    // not sure this test is (any) good
    it('WHEN no argument passed THEN use the global `globalThis`', () => {
      globalThis = {visualViewport: {scale: 42}};
      assert.equal(pinchZoomFactor(), 42);
    });
  });
});
