const template = document.createElement('template');
template.innerHTML = `
  <style>
    :host {
      display: flex;
      overflow: scroll;
      white-space: nowrap;
      scroll-snap-type: x mandatory;
    }
  
    slot {
      display: block;
    }
    
    ::slotted(*) {
      height: 150px;
      width: 150px;
      overflow: hidden;
      object-fit: cover;
      scroll-snap-align: start;
    }
  </style>
  
  <slot></slot>
`;

class PicoImageCarousel extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

const defineCustomElement = () => {
  customElements.define('pico-image-carousel', PicoImageCarousel);
};

defineCustomElement();
