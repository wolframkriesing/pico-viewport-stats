# v2
- [ ] show the CHNAGELOG on the example pages
- [ ] versionize the element?
- [ ] on mobile i have to double click to see the description of the <details>
- [ ] the position=absolute looks aweful on mobile

# v1
- [x] show the scaling factor
- [x] the zoom factor, when the site is pinch zoomed - its in `visualViewport.scale`
- [x] show the width/height 
  - [x] ~~in real pixels~~ what are real pixels :) 
  - [x] in available pixels (e.g. if zoom is 200% a 1000px become 500px)
- [x] show how much white space is left, right and left of <main>
- [x] make it look nice, not just text
- [x] show the width in em
- [x] move repo to pico-elements, so the infra can be reused for new components
- [x] BUG: for some reason it works on picostitch.com but not on the example pages of this repo,
      the main dimensions are often reported 0, which I can also confirm via the `offsetWidth` prop
        WTF: `display:initial` in style.css was causing main's offsetWidth to be 0, no idea ...
        now getting the dimensions using the `load` event works